----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Hongbin Liu
-- 
-- Create Date: 08/05/2016 03:47:59 PM
-- Design Name: CaRIBOU
-- Module Name: h35demo_cfg_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity h35demo_cfg_wrapper is
    generic(
    INPUT_CLK_FRE  : INTEGER := 100_000_000; --INPUT CLOCK SPEED FROM USER LOGIC IN HZ
    CFG_CLK_FREQ   : INTEGER := 50_000_000);   --SPEED THE I2C BUS (SCL) WILL RUN AT IN HZ
    PORT (
    CLK             :IN STD_LOGIC;
    RST             :IN STD_LOGIC;
    
    START_FLG       :IN STD_LOGIC;
    REG_LIMIT       :IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    SHIFT_LIMIT     :IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    OUT_EN          :in std_logic_vector(3 downto 0);   
        
    RAM_ADDR        :IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    RAM_WR_DAT      :IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    RAM_RD_DAT      :OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    RAM_WR_EN       :IN STD_LOGIC;
    RAM_WR_CLK      :IN STD_LOGIC;
            
    SIN             :OUT std_logic_vector(2 downto 0);
    CK1             :OUT std_logic_vector(2 downto 0);
    CK2             :OUT std_logic_vector(2 downto 0);
    LD_NMOS         :OUT STD_LOGIC;
    LD_ANA1         :OUT STD_LOGIC;
    LD_ANA2         :OUT STD_LOGIC;
    LD_CMOS         :OUT STD_LOGIC
     );
end h35demo_cfg_wrapper;

architecture Behavioral of h35demo_cfg_wrapper is

signal h35demo_cfg_ld  :std_logic;
signal ck1_buff  :std_logic;
signal ck2_buff  :std_logic;
signal sin_buff  :std_logic;

begin

h35demo_cfg:entity work.h35demo_cfg_core
generic map(
    input_clk_fre  => INPUT_CLK_FRE, --input clock speed from user logic in Hz
    cfg_clk_freq   =>  CFG_CLK_FREQ  --frequency of the Ck1 and Ck2 in Hz
)
Port map (
    CLK              => CLK,
    RST              => RST,

    START_FLG        => START_FLG,
    REG_LIMIT        => REG_LIMIT,
    SHIFT_LIMIT      => SHIFT_LIMIT,
    OUT_EN           => OUT_EN,

    RAM_WR_EN        => RAM_WR_EN,
    RAM_WR_DAT       => RAM_WR_DAT,
    RAM_ADDR         => RAM_ADDR,
    RAM_RD_DAT       => RAM_RD_DAT,
    RAM_WR_CLK       => RAM_WR_CLK,
           
    SIN              => sin_buff,
    CK1              => ck1_buff,
    CK2              => ck2_buff,
    LD               => h35demo_cfg_ld
 );

LD_NMOS <= h35demo_cfg_ld and OUT_EN(0); 
LD_ANA1 <= h35demo_cfg_ld and OUT_EN(1);
LD_ANA2 <= h35demo_cfg_ld and OUT_EN(2); 
LD_CMOS <= h35demo_cfg_ld and OUT_EN(3);

SIN <= (others => sin_buff);    
CK1 <= (others => ck1_buff);    
CK2 <= (others => ck2_buff);    


end Behavioral;