//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_transceiver.sv
// Description     : CLICpix2 transceiver.
// Author          : Adrian Fiergolski
// Created On      : Mon Apr 24 11:42:37 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module CLICpix2_transceiver #(USE_CHIPSCOPE = 1)
   (
    input logic serialInput_p, serialInput_n,
		
    //Output AXI Stream
    AXI4StreamInterface.master transceiverData,
    CLICpix2_receiver_ClockInterface clock
    
    );

   //Transceiver reset control
   logic 	softReset;
   logic        dontResetOnDataError;
   logic 	pmaReset;
   logic 	dataValid;

   typedef struct {
      logic [15:0] data;
      logic [1:0]  kCode;   
      logic [1:0]  dispErr;
      logic [1:0]  notInTable;} RAWDATA_T;
   
   RAWDATA_T rawData;


   logic 	   rxResetDone;
   (* ASYNC_REG = "TRUE" *) logic rxResetDone_cdrClk; //rxResetDone synchronised with clock.cdr.clk


   logic 	   qpllLock;
   logic 	   qpllRefClkLost;
   logic 	   dataValid_ff; //dataValid after FF (for transceiver reset)
   
   Transceiver transceiver(
			   .soft_reset_rx_in(softReset),
			   .dont_reset_on_data_error_in(dontResetOnDataError),
			   .q0_clk0_gtrefclk_pad_n_in(clock.refClk.n),
			   .q0_clk0_gtrefclk_pad_p_in(clock.refClk.p),
			   .gt0_tx_fsm_reset_done_out(),
			   .gt0_rx_fsm_reset_done_out(clock.cdr.rstN),
			   .gt0_data_valid_in(dataValid_ff),
      
			   .gt0_rxusrclk_out(),
			   .gt0_rxusrclk2_out(clock.cdr.clk),

			   //_________________________________________________________________________
			   //GT0  (X1Y0)
			   //____________________________CHANNEL PORTS________________________________
			   //-------------------------- Channel - DRP Ports  --------------------------
			   .gt0_drpaddr_in                 ('b0), // input wire [8:0] gt0_drpaddr_in
			   .gt0_drpdi_in                   ('b0), // input wire [15:0] gt0_drpdi_in
			   .gt0_drpdo_out                  (), // output wire [15:0] gt0_drpdo_out
			   .gt0_drpen_in                   ('b0), // input wire gt0_drpen_in
			   .gt0_drprdy_out                 (), // output wire gt0_drprdy_out
			   .gt0_drpwe_in                   ('b0), // input wire gt0_drpwe_in
			   //------------------------- Digital Monitor Ports --------------------------
			   .gt0_dmonitorout_out            (), // output wire [7:0] gt0_dmonitorout_out
			   //------------------- RX Initialization and Reset Ports --------------------
			   .gt0_eyescanreset_in            ('b0), // input wire gt0_eyescanreset_in
			   .gt0_rxuserrdy_in               (qpllLock), // input wire gt0_rxuserrdy_in
			   //------------------------ RX Margin Analysis Ports ------------------------
			   .gt0_eyescandataerror_out       (), // output wire gt0_eyescandataerror_out
			   .gt0_eyescantrigger_in          ('b0), // input wire gt0_eyescantrigger_in
			   //---------------- Receive Ports - FPGA RX interface Ports -----------------
			   .gt0_rxdata_out                 (rawData.data), // output wire [15:0] gt0_rxdata_out
			   //---------------- Receive Ports - RX 8B/10B Decoder Ports -----------------
			   .gt0_rxdisperr_out              (rawData.dispErr), // output wire [1:0] gt0_rxdisperr_out
			   .gt0_rxnotintable_out           (rawData.notInTable), // output wire [1:0] gt0_rxnotintable_out
			   //------------------------- Receive Ports - RX AFE -------------------------
			   .gt0_gtxrxp_in                  (serialInput_p), // input wire gt0_gtxrxp_in
			   //---------------------- Receive Ports - RX AFE Ports ----------------------
			   .gt0_gtxrxn_in                  (serialInput_n), // input wire gt0_gtxrxn_in
			   //------------------- Receive Ports - RX Equalizer Ports -------------------
			   .gt0_rxdfelpmreset_in           ('b0), // input wire gt0_rxdfelpmreset_in
			   .gt0_rxmonitorout_out           (), // output wire [6:0] gt0_rxmonitorout_out
			   .gt0_rxmonitorsel_in            ('b0), // input wire [1:0] gt0_rxmonitorsel_in
			   //------------- Receive Ports - RX Fabric Output Control Ports -------------
			   .gt0_rxoutclkfabric_out         (), // output wire gt0_rxoutclkfabric_out
			   //----------- Receive Ports - RX Initialization and Reset Ports ------------
			   .gt0_gtrxreset_in               ('b0), // input wire gt0_gtrxreset_in
			   .gt0_rxpmareset_in              (pmaReset), // input wire gt0_rxpmareset_in
			   //----------------- Receive Ports - RX8B/10B Decoder Ports -----------------
			   .gt0_rxcharisk_out              (rawData.kCode), // output wire [1:0] gt0_rxcharisk_out
			   //------------ Receive Ports -RX Initialization and Reset Ports ------------
			   .gt0_rxresetdone_out            (rxResetDone), // output wire gt0_rxresetdone_out
			   //------------------- TX Initialization and Reset Ports --------------------
			   .gt0_gttxreset_in               ('b0), // input wire gt0_gttxreset_in

			   //____________________________COMMON PORTS________________________________
			   .gt0_qplllock_out(qpllLock),
			   .gt0_qpllrefclklost_out(qpllRefClkLost),
			   .gt0_qplloutclk_out(),
			   .gt0_qplloutrefclk_out(),
			   .sysclk_in(clock.sysClk.clk)
			   );
   
   always_ff @(posedge clock.cdr.clk, negedge rxResetDone) begin : DATA_VALID_FF
      (* ASYNC_REG = "TRUE" *) logic[1:0] rxResetDone_cdrClk_v;
	if (~rxResetDone)
          begin
	     rxResetDone_cdrClk_v <= 2'b00;
             rxResetDone_cdrClk   <= 1'b0;
          end
        else
          begin
	     rxResetDone_cdrClk      <= rxResetDone_cdrClk_v[1];
	     rxResetDone_cdrClk_v[1] <= rxResetDone_cdrClk_v[0];
	     rxResetDone_cdrClk_v[0] <= rxResetDone;
          end
   end // block: DATA_VALID_FF

   always_ff @(posedge clock.cdr.clk) begin : DATA_VALID_FF1
      dataValid_ff <= dataValid;
   end
   
   //Valid data
   always_comb begin : DataValid_comb
      dataValid = 1'b0;
      if( rxResetDone_cdrClk == 1'b1 && rawData.dispErr == 2'b00 && rawData.notInTable == 2'b00 ) begin
	 case(rawData.kCode)
	   2'b00 : 
	     dataValid = 1'b1;                             //data word
	   2'b01 : 
	     if (rawData.data == 16'h50BC || rawData.data == 16'hC5BC ||   //comma pattern
		 rawData.data[7:0] == 8'hF7 )                              //carrier extend
	       dataValid = 1'b1;
	   2'b10 :                                         //carrier extend
	     if( rawData.data[15:8] == 8'hF7 )
	       dataValid = 1'b1;
	   2'b11 :                                         //carrier extend
	     if( rawData.data == 16'hF7F7 )
	       dataValid = 1'b1;
	 endcase // case (rawData.kCode)
      end
   end // block: DataValid_comb

   //AXI assignment
   assign transceiverData.tvalid =( rxResetDone_cdrClk == 1'b1 && rawData.dispErr == 2'b00 && rawData.notInTable == 2'b00) ? 
				  ~ ( rawData.kCode == 2'b01 && (rawData.data == 16'h50BC || rawData.data == 16'hC5BC) ) : 1'b0;
   assign transceiverData.tdata = {rawData.kCode[0], rawData.kCode[1], rawData.data[7:0], rawData.data[15:8] };
   
   generate
      if( USE_CHIPSCOPE == 1) begin : chipscope

	 ////////
	 //VIO
	 ///////

	 (*mark_debug = "TRUE" *) logic softReset_vio;   
	 (*mark_debug = "TRUE" *) logic fsmResetDone_vio;
	 (*mark_debug = "TRUE" *) logic qpllLock_vio;
	 (*mark_debug = "TRUE" *) logic qpllRefClkLost_vio;
	 (*mark_debug = "TRUE" *) logic pmaReset_vio;
	 (*mark_debug = "TRUE" *) logic  dontResetOnDataError_vio;
	 
	 //Inputs
	 assign fsmResetDone_vio = clock.cdr.rstN;
	 assign qpllLock_vio = qpllLock;
	 assign qpllRefClkLost_vio =  qpllRefClkLost;
	 
	 Trsanceiver_control_vio control
	   (.clk(clock.sysClk.clk),
	    .probe_in0(fsmResetDone_vio),
	    .probe_in1(qpllLock_vio),
	    .probe_in2(qpllRefClkLost_vio),
	    .probe_out0(softReset_vio),
	    .probe_out1(dontResetOnDataError_vio),
	    .probe_out2(pmaReset_vio) );

	 //Outputs
	 assign softReset = softReset_vio;
	 assign dontResetOnDataError =  dontResetOnDataError_vio;
	 assign pmaReset = pmaReset_vio;

	 ///////
	 //ILA
	 ///////
	 
	 (*mark_debug = "TRUE" *) logic fsmResetDone_ila;
	 (*mark_debug = "TRUE" *) logic rxResetDone_cdrClk_ila;
	 (*mark_debug = "TRUE" *) RAWDATA_T rawData_ila;
	 (*mark_debug = "TRUE" *) logic dataValid_ila;
	 (*mark_debug = "TRUE" *) logic qpllLock_ila;
	 (*mark_debug = "TRUE" *) logic qpllRefClkLost_ila;
	 
	 
	 //Inputs
	 assign fsmResetDone_ila = clock.cdr.rstN;
	 assign rxResetDone_cdrClk_ila = rxResetDone_cdrClk;
	 assign rawData_ila = rawData;
	 assign dataValid_ila = dataValid;
	 assign qpllLock_ila = qpllLock;
	 assign qpllRefClkLost_ila =  qpllRefClkLost;
	 
	 Transceiver_monitor_ila monitor
	   (.clk(clock.cdr.clk),
	    .probe0(fsmResetDone_ila),
	    .probe1(rxResetDone_cdrClk),
	    .probe2(rawData_ila.data),
	    .probe3(rawData_ila.kCode),
	    .probe4(rawData_ila.dispErr),
	    .probe5(rawData_ila.notInTable),
	    .probe6(dataValid_ila),
	    .probe7(qpllLock_ila),
	    .probe8(qpllRefClkLost_ila) );
	 
      end
      else begin : no_chipscope
	 assign softReset = 'b0;
	 assign dontResetOnDataError = 'b0;
	 assign pmaReset = 'b0;
      end
   endgenerate

   
endmodule // CLICpix2_transceiver


