//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_receiver.s
// Description     : The CLICpix2 receiver.
// Author          : Adrian Fiergolski
// Created On      : Tue Apr 11 21:35:18 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MENTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_receiver_wrapper #(USE_CHIPSCOPE = 1, AXIS_TRANSCEIVER_N = 3, AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 3)
   (
    //Input Clock and data
    input wire 				  refClk_p,
    input wire 				  refClk_n,
   
   
    input wire 				  Transceiver_RX_p,
    input wire 				  Transceiver_RX_n,

    //Input system clock for reset FSM
    input wire 				  sysClk_in,

    //Interrupt signal
    //It generates an interrupt on falling edge of valid data.
    output wire 			  ip2intc_irpt,


    ///////////////////////
    //AXI4-Lite interface
    ///////////////////////

    //Write address channel
    input wire [AXI_ADDR_WIDTH-1 : 0] 	  awaddr,
    input wire [2 : 0] 			  awprot,
    input wire 				  awvalid,
    output wire 			  awready,

    //Write data channel
    input wire [AXI_DATA_WIDTH-1 : 0] 	  wdata,
    input wire [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
    input wire 				  wvalid,
    output wire 			  wready,
   
    //Write response channel
    output wire [1 : 0] 		  bresp,
    output wire 			  bvalid,
    input wire 				  bready,

    //Read address channel
    input wire [AXI_ADDR_WIDTH-1 : 0] 	  araddr,
    input wire [2 : 0] 			  arprot,
    input wire 				  arvalid,
    output wire 			  arready,

    //Read data channel
    output wire [AXI_DATA_WIDTH-1 : 0] 	  rdata,
    output wire [1 : 0] 		  rresp,
    output wire 			  rvalid,
    input wire 				  rready,
   
    input wire 				  aclk,
    input wire 				  aresetN
    );

   CLICpix2_receiver #(.USE_CHIPSCOPE(USE_CHIPSCOPE), .AXIS_TRANSCEIVER_N(AXIS_TRANSCEIVER_N),
		       .AXI_DATA_WIDTH(AXI_DATA_WIDTH), .AXI_ADDR_WIDTH(AXI_ADDR_WIDTH) ) receiver( .refClk_p(refClk_p),.refClk_n(refClk_n),
												    .Transceiver_RX_p(Transceiver_RX_p), .Transceiver_RX_n(Transceiver_RX_n),
												    
												    .sysClk_in(sysClk_in),
												    
												    //AXI4-Lite output
												    .awaddr(awaddr),
												    .awprot(awprot),
												    .awvalid(awvalid),
												    .awready(awready),

												    .wdata(wdata),
												    .wstrb(wstrb),
												    .wvalid(wvalid),
												    .wready(wready),
												    
												    .bresp(bresp),
												    .bvalid(bvalid),
												    .bready(bready),

												    .araddr(araddr),
												    .arprot(arprot),
												    .arvalid(arvalid),
												    .arready(arready),

												    .rdata(rdata),
												    .rresp(rresp),
												    .rvalid(rvalid),
												    .rready(rready),


												    
												    .aclk(aclk),
												    .aresetN(aresetN)
												    );
   
endmodule // CLICpix2_receiver_wrapper
