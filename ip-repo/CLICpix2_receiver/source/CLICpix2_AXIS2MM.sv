//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_AXIS2MM.sv
// Description     : The module translates CLICpix2 AXI4-Stream to AXI4-Lite Memory Mapped interface.
// Author          : Adrian Fiergolski
// Created On      : Tue Apr 25 19:06:09 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.


`timescale 1 ns / 1 ps

module CLICpix2_AXIS2MM 
  (
   //Input
   AXI4StreamInterface.slave fifoStream,
   input logic [31:0] fifoCounter,

   //Output
   AXI4LiteInterface.slave axi
   );


   typedef enum	{IDLE, READ_OK, READ_ERR } STATE_T;
   STATE_T state, state_n;

   //Register storing output data
   struct{
      logic en;
      logic [$size(axi.rdata)-1 : 0] d;
      logic [$size(axi.rdata)-1 : 0] q; } regOut;
   

   always_comb begin: COMB_FSM

      //Defaults
      axi.arready = 1'b1; //address is accepted immediately

      axi.rdata = 'x;    //don't care values
      axi.rvalid = 1'b0;
      axi.rresp = axi.OKAY;
      regOut.en = 1'b0;
      regOut.d = 'hBADFACE;
      fifoStream.tready = 1'b0;
      
      //Write operation are not supported
      axi.awready = '1;
      axi.wready = '1;
      axi.bresp = axi.DECERR;
      axi.bvalid = '1;
      
      state_n = state;
      
      case (state)
	IDLE : begin
	   if(axi.arvalid) begin

	      regOut.en = 1'b1;
	      
	      case(axi.araddr[2])
		1'b0: begin                       //FIFO readout
		   if(fifoStream.tvalid) begin //there is something in the FIFO
		      regOut.d = {1'b1, 13'b0, fifoStream.tdata};
		      fifoStream.tready = 1'b1;
		   end
		   else begin
		      regOut.d = {2'b01, 30'b0};
		   end

		   state_n = READ_OK;
		end
		1'b1: begin                        //counter readout
		   regOut.d = fifoCounter;

		   state_n = READ_OK;
		end
		default : begin
		   axi.rvalid = 1'b1;
		   axi.rresp = axi.DECERR;

		   state_n = READ_ERR;
		end
		
	      endcase // case (axi.arready)
	      
	   end // if (axi.arvalid)
	end // case: IDLE
	
	READ_OK : begin
	   axi.rvalid = 1'b1;
	   axi.rdata = regOut.q;

	   if(axi.rready)  //wait until handshake
	     state_n = IDLE;
	end

	READ_ERR : begin
	   axi.rvalid = 1'b1;
	   axi.rresp = axi.DECERR;

	   if(axi.rready)  //wait until handshake
	     state_n = IDLE;
	end
      endcase // case (state)
   end // block: COMB_FSM
   
   always_ff @(posedge axi.aclk, negedge axi.aresetN) begin : SEQ_FSM
      if ( ~ axi.aresetN ) begin
	 state <= IDLE;
      end
      else begin
	 state <= state_n;
	 
	 if(regOut.en) //store the output register
	   regOut.q <= regOut.d;
      end
   end // block: SEQ_FSM
   
endmodule
