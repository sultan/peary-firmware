create_clock -name CLICpix2_slow_clock -period 10.00 -waveform {1 6} [get_ports CLICpix2_slow_clock]
create_clock -name aclk -period 5.00 [get_ports aclk]
create_clock -name TLU_clk -period 10.00 -waveform {1 6} [get_ports TLU_clk]
set_clock_groups -asynchronous -group {aclk} -group {CLICpix2_slow_clock TLU_clk}
