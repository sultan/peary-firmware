//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_waveGenerator.sv
// Description     : The module generates waveforms to stimulate CLICpix2 pins.
// Author          : Adrian Fiergolski
// Created On      : Mon May  8 15:25:25 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module CLICpix2_waveGenerator
  (
   CLICpix2_waveGeneratorControlInterface waveControl,
   CLICpix2_pinsInterface CLICpix2_pins,
   FIFO_interface timestampsFIFO,
   TLU_interface tlu
   );

   
   typedef enum {IDLE, EVENTS} STATE_T;

   assign timestampsFIFO.wr_clk = waveControl.clk;
   
   always_ff @(posedge waveControl.clk, negedge waveControl.resetN) begin : MOORE_FSM
      logic [$bits(waveControl.EVENT_T.duration)-1:0]  timeCnt; //counts passing time
      logic [$clog2( $size(waveControl.events) ) -1 : 0] eventCnt; //counts events
      logic 						 en_d; //delayed enable
      STATE_T state;
      
      if( ~ waveControl.resetN ) begin 
	 state <= IDLE;
	 timeCnt <= 1;
	 eventCnt <= '0;
	 en_d <= '0;
	 
	 CLICpix2_pins.tp_sw <= 1'b0;
	 CLICpix2_pins.shutter <= 1'b0;
	 CLICpix2_pins.pwr_pulse <= 1'b0;
      end
      else begin
	 //default values (disable)
	 CLICpix2_pins.tp_sw <= 1'b0;
	 CLICpix2_pins.shutter <= 1'b0;
	 CLICpix2_pins.pwr_pulse <= 1'b0;
	 en_d <= waveControl.en;

	 //Timestamps FIFO
	 timestampsFIFO.din_en <= 1'b0;
	 timestampsFIFO.din <=  {waveControl.events[eventCnt].tp_sw,
				 waveControl.events[eventCnt].shutter,
				 waveControl.events[eventCnt].pwr_pulse,
				 tlu.counter};
	 case(state)
	   IDLE : begin
	      timeCnt <= 1;
	      eventCnt <= '0;
	      if( {waveControl.en, en_d} == 2'b10 ) begin //rising edge of the enable pin
		 state <= EVENTS;
	      end
	   end

	   EVENTS : begin
	      timeCnt <= timeCnt+1;  //time advance
	      
	      if(timeCnt == 1) //store timestamp
		 timestampsFIFO.din_en <= 1'b1;

	      //assign outputs
	      CLICpix2_pins.tp_sw <= waveControl.events[eventCnt].tp_sw;
	      CLICpix2_pins.shutter <= waveControl.events[eventCnt].shutter;
	      CLICpix2_pins.pwr_pulse <= waveControl.events[eventCnt].pwr_pulse;

	      if(waveControl.events[eventCnt].duration == 0) begin  //it's the last event
		 if(waveControl.loop) begin
		    timeCnt <= 1;
		    eventCnt <= '0;
		    state <= EVENTS;
		 end
		 else
		   state <= IDLE;
	      end
	      else if( timeCnt == waveControl.events[eventCnt].duration ) begin //there is next event
		 timeCnt <= 1;
		 eventCnt <= eventCnt + 1;
		 state <= EVENTS;
	      end // else: !if(waveControl.events[eventCnt].nextEventTime == 0)
	   end // case: EVENTS
	   endcase // case (state)
      end // else: !if( ~ waveControl.resetN )
   end // block: MOORE_FSM

endmodule // CLICpix2_waveGenerator

   
   
