//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_control_wrapper.v
// Description     : The CLICpix2 control Verilog wrapper.
// Author          : Adrian Fiergolski
// Created On      : Tue May  2 16:08:17 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_control_wrapper  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 8, WAVE_CONTROL_EVENTS_NUMBER = 32, USE_CHIPSCOPE = 1)
   (    
        output wire 			      CLICpix2_reset_p, CLICpix2_reset_n,
	output wire 			      CLICpix2_pwr_pulse_p, CLICpix2_pwr_pulse_n,
	output wire 			      CLICpix2_shutter_p, CLICpix2_shutter_n,
	output wire 			      CLICpix2_tp_sw_p, CLICpix2_tp_sw_n,

	input wire 			      CLICpix2_slow_clock, 

	
	output wire 			      C3PD_reset_p, C3PD_reset_n,

	//TLU interface
	input wire 			      TLU_clk,
	input wire 			      TLU_trigger_p,
	input wire 			      TLU_trigger_n,
	output wire 			      TLU_busy_p,
	output wire 			      TLU_busy_n,
	input wire 			      TLU_reset_p,
	input wire 			      TLU_reset_n,
	
	//Interrupt
	//It generates an interrupt (rising edge) on falling edge of the shutter.
	output wire 			      ip2intc_irpt, 

	///////////////////////
	//AXI4-Lite interface
	///////////////////////

	//Write address channel
	input wire [AXI_ADDR_WIDTH-1 : 0]     awaddr,
	input wire [2 : 0] 		      awprot,
	input wire 			      awvalid,
	output wire 			      awready,

	//Write data channel
	input wire [AXI_DATA_WIDTH-1 : 0]     wdata,
	input wire [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
	input wire 			      wvalid,
	output wire 			      wready,
   
	//Write response channel
	output wire [1 : 0] 		      bresp,
	output wire 			      bvalid,
	input wire 			      bready,

	//Read address channel
	input wire [AXI_ADDR_WIDTH-1 : 0]     araddr,
	input wire [2 : 0] 		      arprot,
	input wire 			      arvalid,
	output wire 			      arready,

	//Read data channel
	output wire [AXI_DATA_WIDTH-1 : 0]    rdata,
	output wire [1 : 0] 		      rresp,
	output wire 			      rvalid,
	input wire 			      rready,
   
	input wire 			      aclk,
	input wire 			      aresetN

    );

   CLICpix2_control #(.AXI_DATA_WIDTH(AXI_DATA_WIDTH), .AXI_ADDR_WIDTH(AXI_ADDR_WIDTH), 
		      .WAVE_CONTROL_EVENTS_NUMBER(WAVE_CONTROL_EVENTS_NUMBER), .USE_CHIPSCOPE(USE_CHIPSCOPE) )
   control(
           .CLICpix2_reset_p(CLICpix2_reset_p), .CLICpix2_reset_n(CLICpix2_reset_n),
	   .CLICpix2_pwr_pulse_p(CLICpix2_pwr_pulse_p), .CLICpix2_pwr_pulse_n(CLICpix2_pwr_pulse_n),
	   .CLICpix2_shutter_p(CLICpix2_shutter_p), .CLICpix2_shutter_n(CLICpix2_shutter_n),
	   .CLICpix2_tp_sw_p(CLICpix2_tp_sw_p), .CLICpix2_tp_sw_n(CLICpix2_tp_sw_n),

	   .CLICpix2_slow_clock(CLICpix2_slow_clock),
	   
	   .C3PD_reset_p(C3PD_reset_p), .C3PD_reset_n(C3PD_reset_n),

	   //TLU interface
	   .TLU_clk(TLU_clk),
	   .TLU_trigger_p(TLU_trigger_p), .TLU_trigger_n(TLU_trigger_n),
	   .TLU_busy_p(TLU_busy_p), .TLU_busy_n(TLU_busy_n),
	   .TLU_reset_p(TLU_reset_p), .TLU_reset_n(TLU_reset_n),
	   
	   .ip2intc_irpt(ip2intc_irpt),

	   //AXI4-Lite interface

	   //Write address channel
	   .awaddr(awaddr),
	   .awprot(awprot),
	   .awvalid(awvalid),
	   .awready(awready),

	   //Write data channel
	   .wdata(wdata),
	   .wstrb(wstrb),
	   .wvalid(wvalid),
	   .wready(wready),
      
	   //Write response channel
	   .bresp(bresp),
	   .bvalid(bvalid),
	   .bready(bready),

	   //Read address channel
	   .araddr(araddr),
	   .arprot(arprot),
	   .arvalid(arvalid),
	   .arready(arready),

	   //Read data channel
	   .rdata(rdata),
	   .rresp(rresp),
	   .rvalid(rvalid),
	   .rready(rready),
      
	   .aclk(aclk),
	   .aresetN(aresetN) );
   
endmodule // CLICpix2_control_wrapper


