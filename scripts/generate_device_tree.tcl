set workdir "../caribou-soc/hsi"
set outputdir "../../outputs/dts"
set caribouDeviceTree "../../device-tree-caribou"
set HDF "../../outputs/caribou_top.hdf"

file mkdir $workdir
cd ${workdir}
if { ! [file exist "device-tree-xlnx"] } {
    exec -ignorestderr git clone https://github.com/adrianf0/device-tree-xlnx.git
}
file copy -force ${HDF} .
open_hw_design [file tail ${HDF}]
set_repo_path $caribouDeviceTree
set_repo_path device-tree-xlnx
create_sw_design device-tree -os device_tree -proc ps7_cortexa9_0
set_property CONFIG.periph_type_overrides "{BOARD zc706}" [get_os]
generate_target 
file copy -force {*}[glob *.dts] {*}[glob *.dtsi] ${outputdir}
exit
