set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets CLICpix2_SPI_transceiver_rx]


#TLU signals
#set_property PACKAGE_PIN AG21 [get_ports TLU_CLK_p]
#set_property PACKAGE_PIN AH21 [get_ports TLU_CLK_n]
#set_property PACKAGE_PIN AH23 [get_ports TLU_TRG_p]
#set_property PACKAGE_PIN AH24 [get_ports TLU_TRG_n]
#set_property PACKAGE_PIN AD21 [get_ports TLU_BSY_p]
#set_property PACKAGE_PIN AE21 [get_ports TLU_BSY_n]
#set_property PACKAGE_PIN AA22 [get_ports TLU_RST_p]
#set_property PACKAGE_PIN AA23 [get_ports TLU_RST_n]

#Clock signal from SI5345
set_property PACKAGE_PIN AE22 [get_ports SI5345_CLK_OUT8_clk_p]
set_property PACKAGE_PIN AF22 [get_ports SI5345_CLK_OUT8_clk_n]
set_property DIFF_TERM true [get_ports SI5345_CLK_OUT8_*]
set_property IOSTANDARD LVDS_25 [get_ports SI5345_CLK_OUT8_*]

set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_wiz_1_transceiver_rx]

# Matrix 2 triggered

set_property PACKAGE_PIN P25 [get_ports ext_sin_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_N[0]]

set_property PACKAGE_PIN W25 [get_ports ext_ck1_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_N[0]]

set_property PACKAGE_PIN Y26 [get_ports ext_ck2_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_P[0]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_N[0]]


#Matrix 1 
set_property PACKAGE_PIN R25 [get_ports ext_sin_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_N[1]]

set_property PACKAGE_PIN T30 [get_ports ext_ck1_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_N[1]]

set_property PACKAGE_PIN T29 [get_ports ext_ck2_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_P[1]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_N[1]]


#Matrix 1 ISO
set_property PACKAGE_PIN P23 [get_ports ext_sin_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_sin_diff_N[2]]

set_property PACKAGE_PIN N26 [get_ports ext_ck1_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck1_diff_N[2]]

set_property PACKAGE_PIN P21 [get_ports ext_ck2_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_P[2]]
set_property IOSTANDARD LVDS_25 [get_ports ext_ck2_diff_N[2]]

#Load signals

set_property PACKAGE_PIN Y30 [get_ports ext_ldM2_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM2_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM2_diff_N]

set_property PACKAGE_PIN R28 [get_ports ext_ldM1_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM1_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM1_diff_N]

set_property PACKAGE_PIN AG26 [get_ports ext_ldM1ISO_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM1ISO_diff_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ldM1ISO_diff_N]

set_property PACKAGE_PIN AG22 [get_ports ATLASPix_PLL_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_PLL_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_PLL_CLK_N]



#M2
set_property PACKAGE_PIN AH19 [get_ports ext_ATLASPix_Sync_reset_M2_P]
set_property PACKAGE_PIN AJ19 [get_ports ext_ATLASPix_Sync_reset_M2_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M2_N]

set_property PACKAGE_PIN AF15 [get_ports ext_ATLASPix_Trigger_M2_P]
set_property PACKAGE_PIN AG15 [get_ports ext_ATLASPix_TriggerT_M2_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M2_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M2_N]

#M1
set_property PACKAGE_PIN AJ15 [get_ports ext_ATLASPix_Sync_reset_M1_P]
set_property PACKAGE_PIN AK15 [get_ports ext_ATLASPix_Sync_reset_M1_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M1_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M1_N]

set_property PACKAGE_PIN AJ15 [get_ports ext_ATLASPix_Trigger_M1_P]
set_property PACKAGE_PIN AK15 [get_ports ext_ATLASPix_Trigger_M1_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1_N]


#M1ISO
set_property PACKAGE_PIN AE18 [get_ports ext_ATLASPix_Sync_reset_M1ISO_P]
set_property PACKAGE_PIN AE17 [get_ports ext_ATLASPix_Sync_reset_M1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Sync_reset_M1ISO_N]

set_property PACKAGE_PIN AB15 [get_ports ext_ATLASPix_Trigger_M1ISO_P]
set_property PACKAGE_PIN AB14 [get_ports ext_ATLASPix_TriggerT_M1ISO_N]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1ISO_P]
set_property IOSTANDARD LVDS_25 [get_ports ext_ATLASPix_Trigger_M1ISO_N]


#set_property PACKAGE_PIN AJ18 [get_ports fmc_i2c_sda_io]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_i2c_sda_io]

#set_property PACKAGE_PIN AJ14 [get_ports fmc_i2c_scl_io]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_i2c_scl_io]


#DATA from Matrices


#M2
#set_property PACKAGE_PIN AC13 [get_ports ATLASPix_DATA_IN_rxp]
#set_property PACKAGE_PIN AC14 [get_ports ATLASPix_DATA_IN_rxn]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_rxp]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_rxn]

##M1
#set_property PACKAGE_PIN AC13 [get_ports ATLASPix_DATA_IN_P]
#set_property PACKAGE_PIN AC14 [get_ports ATLASPix_DATA_IN_N]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_P]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_N]

##M1ISO
#set_property PACKAGE_PIN AC13 [get_ports ATLASPix_DATA_IN_P]
#set_property PACKAGE_PIN AC14 [get_ports ATLASPix_DATA_IN_N]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_P]
#set_property IOSTANDARD LVDS_25 [get_ports ATLASPix_DATA_IN_N]









