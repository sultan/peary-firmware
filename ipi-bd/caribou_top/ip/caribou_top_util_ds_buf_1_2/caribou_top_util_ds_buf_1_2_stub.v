// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Tue Sep 19 11:47:24 2017
// Host        : pcunigefelix.dyndns.cern.ch running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
// Command     : write_verilog -force -mode synth_stub -rename_top caribou_top_util_ds_buf_1_2 -prefix
//               caribou_top_util_ds_buf_1_2_ caribou_top_util_ds_buf_1_0_stub.v
// Design      : caribou_top_util_ds_buf_1_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "util_ds_buf,Vivado 2017.2" *)
module caribou_top_util_ds_buf_1_2(OBUF_IN, OBUF_DS_P, OBUF_DS_N)
/* synthesis syn_black_box black_box_pad_pin="OBUF_IN[2:0],OBUF_DS_P[2:0],OBUF_DS_N[2:0]" */;
  input [2:0]OBUF_IN;
  output [2:0]OBUF_DS_P;
  output [2:0]OBUF_DS_N;
endmodule
