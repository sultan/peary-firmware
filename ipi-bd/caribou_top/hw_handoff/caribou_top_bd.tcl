
################################################################
# This is a generated script based on design: caribou_top
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2017.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source caribou_top_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z045ffg900-2
   set_property BOARD_PART xilinx.com:zc706:part0:1.3 [current_project]
}


# CHANGE DESIGN NAME HERE
set design_name caribou_top

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir ./ipi-bd

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: output_SC
proc create_hier_cell_output_SC { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_output_SC() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN
  create_bd_pin -dir I -from 2 -to 0 -type clk ck1_array
  create_bd_pin -dir O -from 2 -to 0 -type clk ck1_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk ck1_array_P
  create_bd_pin -dir I -from 2 -to 0 -type clk ck2_array
  create_bd_pin -dir O -from 2 -to 0 -type clk ck2_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk ck2_array_P
  create_bd_pin -dir I -from 0 -to 0 -type clk ld_m1
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1_P
  create_bd_pin -dir I -from 0 -to 0 -type clk ld_m1iso
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1iso_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1iso_P
  create_bd_pin -dir I -from 0 -to 0 -type clk ld_m2
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m2_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m2_P
  create_bd_pin -dir O -from 0 -to 0 -type clk pll_clk_N
  create_bd_pin -dir O -from 0 -to 0 -type clk pll_clk_P
  create_bd_pin -dir I -from 2 -to 0 -type clk sin_array
  create_bd_pin -dir O -from 2 -to 0 -type clk sin_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk sin_array_P

  # Create instance: sin_ds_buffer, and set properties
  set sin_ds_buffer [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 sin_ds_buffer ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.C_SIZE {3} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $sin_ds_buffer

  # Create instance: util_ds_buf_0, and set properties
  set util_ds_buf_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_0 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_0

  # Create instance: util_ds_buf_1, and set properties
  set util_ds_buf_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_1 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_1

  # Create instance: util_ds_buf_3, and set properties
  set util_ds_buf_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_3 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.C_SIZE {3} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_3

  # Create instance: util_ds_buf_4, and set properties
  set util_ds_buf_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_4 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.C_SIZE {3} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_4

  # Create instance: util_ds_buf_5, and set properties
  set util_ds_buf_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_5 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_5

  # Create instance: util_ds_buf_6, and set properties
  set util_ds_buf_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_6 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
CONFIG.DIFF_CLK_IN_BOARD_INTERFACE {Custom} \
 ] $util_ds_buf_6

  # Create port connections
  connect_bd_net -net ATLASPix_Config_0_ext_ck1 [get_bd_pins ck1_array] [get_bd_pins util_ds_buf_3/OBUF_IN]
  connect_bd_net -net ATLASPix_Config_0_ext_ck2 [get_bd_pins sin_array] [get_bd_pins sin_ds_buffer/OBUF_IN]
  connect_bd_net -net ATLASPix_Config_0_ext_ldm1 [get_bd_pins ld_m2] [get_bd_pins util_ds_buf_5/OBUF_IN]
  connect_bd_net -net ATLASPix_Config_0_ext_ldm2 [get_bd_pins ld_m1iso] [get_bd_pins util_ds_buf_1/OBUF_IN]
  connect_bd_net -net ATLASPix_Config_0_ext_sin [get_bd_pins ck2_array] [get_bd_pins util_ds_buf_4/OBUF_IN]
  connect_bd_net -net OBUF_IN_1 [get_bd_pins OBUF_IN] [get_bd_pins util_ds_buf_6/OBUF_IN]
  connect_bd_net -net clk_wiz_0_ATLASPix_PLL_clk [get_bd_pins ld_m1] [get_bd_pins util_ds_buf_0/OBUF_IN]
  connect_bd_net -net util_ds_buf_0_OBUF_DS_N [get_bd_pins ld_m1_N] [get_bd_pins util_ds_buf_0/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_0_OBUF_DS_P [get_bd_pins ld_m1_P] [get_bd_pins util_ds_buf_0/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_1_OBUF_DS_N [get_bd_pins ld_m1iso_N] [get_bd_pins util_ds_buf_1/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_1_OBUF_DS_P [get_bd_pins ld_m1iso_P] [get_bd_pins util_ds_buf_1/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_2_OBUF_DS_N [get_bd_pins sin_array_N] [get_bd_pins sin_ds_buffer/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_2_OBUF_DS_P [get_bd_pins sin_array_P] [get_bd_pins sin_ds_buffer/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_3_OBUF_DS_N [get_bd_pins ck1_array_N] [get_bd_pins util_ds_buf_3/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_3_OBUF_DS_P [get_bd_pins ck1_array_P] [get_bd_pins util_ds_buf_3/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_4_OBUF_DS_N [get_bd_pins ck2_array_N] [get_bd_pins util_ds_buf_4/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_4_OBUF_DS_P [get_bd_pins ck2_array_P] [get_bd_pins util_ds_buf_4/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_5_OBUF_DS_N [get_bd_pins ld_m2_N] [get_bd_pins util_ds_buf_5/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_5_OBUF_DS_P [get_bd_pins ld_m2_P] [get_bd_pins util_ds_buf_5/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_6_OBUF_DS_N [get_bd_pins pll_clk_N] [get_bd_pins util_ds_buf_6/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_6_OBUF_DS_P [get_bd_pins pll_clk_P] [get_bd_pins util_ds_buf_6/OBUF_DS_P]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ATLASPix_SR_FSM_top
proc create_hier_cell_ATLASPix_SR_FSM_top { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ATLASPix_SR_FSM_top() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 -type clk OBUF_IN
  create_bd_pin -dir O -from 2 -to 0 -type clk ck1_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk ck1_array_P
  create_bd_pin -dir O -from 2 -to 0 -type clk ck2_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk ck2_array_P
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1_P
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1iso_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m1iso_P
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m2_N
  create_bd_pin -dir O -from 0 -to 0 -type clk ld_m2_P
  create_bd_pin -dir O -from 0 -to 0 -type clk pll_clk_N
  create_bd_pin -dir O -from 0 -to 0 -type clk pll_clk_P
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn
  create_bd_pin -dir O -from 2 -to 0 -type clk sin_array_N
  create_bd_pin -dir O -from 2 -to 0 -type clk sin_array_P

  # Create instance: ATLASPix_SR_FSM_0, and set properties
  set ATLASPix_SR_FSM_0 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_SR_FSM:1.0 ATLASPix_SR_FSM_0 ]

  # Create instance: output_SC
  create_hier_cell_output_SC $hier_obj output_SC

  # Create interface connections
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins ATLASPix_SR_FSM_0/S00_AXI]

  # Create port connections
  connect_bd_net -net OBUF_IN_1 [get_bd_pins OBUF_IN] [get_bd_pins output_SC/OBUF_IN]
  connect_bd_net -net ck1_array_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_ck1] [get_bd_pins output_SC/ck1_array]
  connect_bd_net -net ck2_array_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_ck2] [get_bd_pins output_SC/ck2_array]
  connect_bd_net -net ld_m1_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_ldm1] [get_bd_pins output_SC/ld_m1]
  connect_bd_net -net ld_m1iso_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_ldm1iso] [get_bd_pins output_SC/ld_m1iso]
  connect_bd_net -net ld_m2_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_ldm2] [get_bd_pins output_SC/ld_m2]
  connect_bd_net -net output_SC_ck1_array_N [get_bd_pins ck1_array_N] [get_bd_pins output_SC/ck1_array_N]
  connect_bd_net -net output_SC_ck1_array_P [get_bd_pins ck1_array_P] [get_bd_pins output_SC/ck1_array_P]
  connect_bd_net -net output_SC_ck2_array_N [get_bd_pins ck2_array_N] [get_bd_pins output_SC/ck2_array_N]
  connect_bd_net -net output_SC_ck2_array_P [get_bd_pins ck2_array_P] [get_bd_pins output_SC/ck2_array_P]
  connect_bd_net -net output_SC_ld_m1_N [get_bd_pins ld_m1_N] [get_bd_pins output_SC/ld_m1_N]
  connect_bd_net -net output_SC_ld_m1_P [get_bd_pins ld_m1_P] [get_bd_pins output_SC/ld_m1_P]
  connect_bd_net -net output_SC_ld_m1iso_N [get_bd_pins ld_m1iso_N] [get_bd_pins output_SC/ld_m1iso_N]
  connect_bd_net -net output_SC_ld_m1iso_P [get_bd_pins ld_m1iso_P] [get_bd_pins output_SC/ld_m1iso_P]
  connect_bd_net -net output_SC_ld_m2_N [get_bd_pins ld_m2_N] [get_bd_pins output_SC/ld_m2_N]
  connect_bd_net -net output_SC_ld_m2_P [get_bd_pins ld_m2_P] [get_bd_pins output_SC/ld_m2_P]
  connect_bd_net -net output_SC_pll_clk_N [get_bd_pins pll_clk_N] [get_bd_pins output_SC/pll_clk_N]
  connect_bd_net -net output_SC_pll_clk_P [get_bd_pins pll_clk_P] [get_bd_pins output_SC/pll_clk_P]
  connect_bd_net -net output_SC_sin_array_N [get_bd_pins sin_array_N] [get_bd_pins output_SC/sin_array_N]
  connect_bd_net -net output_SC_sin_array_P [get_bd_pins sin_array_P] [get_bd_pins output_SC/sin_array_P]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins s00_axi_aclk] [get_bd_pins ATLASPix_SR_FSM_0/s00_axi_aclk]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins s00_axi_aresetn] [get_bd_pins ATLASPix_SR_FSM_0/s00_axi_aresetn]
  connect_bd_net -net sin_array_1 [get_bd_pins ATLASPix_SR_FSM_0/ext_sin] [get_bd_pins output_SC/sin_array]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ATLASPix_FastControl
proc create_hier_cell_ATLASPix_FastControl { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ATLASPix_FastControl() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 CLK_IN_D
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 CLK_IN_D1
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 CLK_IN_D2
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI1
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI2

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N1
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_N2
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P1
  create_bd_pin -dir O -from 0 -to 0 -type clk OBUF_DS_P2
  create_bd_pin -dir I -from 31 -to 0 ext_ATLASPix_triggerCount
  create_bd_pin -dir I -from 31 -to 0 ext_ATLASPix_triggerCount1
  create_bd_pin -dir I -from 31 -to 0 ext_ATLASPix_triggerCount2
  create_bd_pin -dir I -from 31 -to 0 ext_ATLASPix_triggerCount3
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: ATLASPix_FastControl_0, and set properties
  set ATLASPix_FastControl_0 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_FastControl:1.0 ATLASPix_FastControl_0 ]

  # Create instance: ATLASPix_FastControl_1, and set properties
  set ATLASPix_FastControl_1 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_FastControl:1.0 ATLASPix_FastControl_1 ]

  # Create instance: ATLASPix_FastControl_2, and set properties
  set ATLASPix_FastControl_2 [ create_bd_cell -type ip -vlnv user.org:user:ATLASPix_FastControl:1.0 ATLASPix_FastControl_2 ]

  # Create instance: util_ds_buf_1, and set properties
  set util_ds_buf_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_1 ]

  # Create instance: util_ds_buf_2, and set properties
  set util_ds_buf_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_2 ]

  # Create instance: util_ds_buf_3, and set properties
  set util_ds_buf_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_3 ]

  # Create instance: util_ds_buf_4, and set properties
  set util_ds_buf_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_4 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
 ] $util_ds_buf_4

  # Create instance: util_ds_buf_5, and set properties
  set util_ds_buf_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_5 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
 ] $util_ds_buf_5

  # Create instance: util_ds_buf_6, and set properties
  set util_ds_buf_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_6 ]
  set_property -dict [ list \
CONFIG.C_BUF_TYPE {OBUFDS} \
 ] $util_ds_buf_6

  # Create interface connections
  connect_bd_intf_net -intf_net CLK_IN_D_1 [get_bd_intf_pins CLK_IN_D2] [get_bd_intf_pins util_ds_buf_1/CLK_IN_D]
  connect_bd_intf_net -intf_net CLK_IN_D_1_1 [get_bd_intf_pins CLK_IN_D] [get_bd_intf_pins util_ds_buf_2/CLK_IN_D]
  connect_bd_intf_net -intf_net CLK_IN_D_2_1 [get_bd_intf_pins CLK_IN_D1] [get_bd_intf_pins util_ds_buf_3/CLK_IN_D]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M03_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins ATLASPix_FastControl_0/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M04_AXI [get_bd_intf_pins S00_AXI1] [get_bd_intf_pins ATLASPix_FastControl_1/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M05_AXI [get_bd_intf_pins S00_AXI2] [get_bd_intf_pins ATLASPix_FastControl_2/S00_AXI]

  # Create port connections
  connect_bd_net -net ATLASPix_FastControl_0_ext_ATLASPix_Sync [get_bd_pins ATLASPix_FastControl_0/ext_ATLASPix_Sync] [get_bd_pins util_ds_buf_5/OBUF_IN]
  connect_bd_net -net ATLASPix_FastControl_1_ext_ATLASPix_Sync [get_bd_pins ATLASPix_FastControl_1/ext_ATLASPix_Sync] [get_bd_pins util_ds_buf_6/OBUF_IN]
  connect_bd_net -net ATLASPix_FastControl_2_ext_ATLASPix_Sync [get_bd_pins ATLASPix_FastControl_2/ext_ATLASPix_Sync] [get_bd_pins util_ds_buf_4/OBUF_IN]
  connect_bd_net -net ext_ATLASPix_triggerCount1_1 -boundary_type upper [get_bd_pins ext_ATLASPix_triggerCount1]
  connect_bd_net -net ext_ATLASPix_triggerCount2_1 -boundary_type upper [get_bd_pins ext_ATLASPix_triggerCount2]
  connect_bd_net -net ext_ATLASPix_triggerCount3_1 -boundary_type upper [get_bd_pins ext_ATLASPix_triggerCount3]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins s00_axi_aclk] [get_bd_pins ATLASPix_FastControl_0/s00_axi_aclk] [get_bd_pins ATLASPix_FastControl_1/s00_axi_aclk] [get_bd_pins ATLASPix_FastControl_2/s00_axi_aclk]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins s00_axi_aresetn] [get_bd_pins ATLASPix_FastControl_0/s00_axi_aresetn] [get_bd_pins ATLASPix_FastControl_1/s00_axi_aresetn] [get_bd_pins ATLASPix_FastControl_2/s00_axi_aresetn]
  connect_bd_net -net util_ds_buf_1_IBUF_OUT [get_bd_pins ATLASPix_FastControl_0/ext_ATLASPix_Trigger] [get_bd_pins util_ds_buf_1/IBUF_OUT]
  connect_bd_net -net util_ds_buf_2_IBUF_OUT [get_bd_pins ATLASPix_FastControl_1/ext_ATLASPix_Trigger] [get_bd_pins util_ds_buf_2/IBUF_OUT]
  connect_bd_net -net util_ds_buf_3_IBUF_OUT [get_bd_pins ATLASPix_FastControl_2/ext_ATLASPix_Trigger] [get_bd_pins util_ds_buf_3/IBUF_OUT]
  connect_bd_net -net util_ds_buf_4_OBUF_DS_N [get_bd_pins OBUF_DS_N1] [get_bd_pins util_ds_buf_4/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_4_OBUF_DS_P [get_bd_pins OBUF_DS_P1] [get_bd_pins util_ds_buf_4/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_5_OBUF_DS_N [get_bd_pins OBUF_DS_N2] [get_bd_pins util_ds_buf_5/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_5_OBUF_DS_P [get_bd_pins OBUF_DS_P2] [get_bd_pins util_ds_buf_5/OBUF_DS_P]
  connect_bd_net -net util_ds_buf_6_OBUF_DS_N [get_bd_pins OBUF_DS_N] [get_bd_pins util_ds_buf_6/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_6_OBUF_DS_P [get_bd_pins OBUF_DS_P] [get_bd_pins util_ds_buf_6/OBUF_DS_P]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]
  set ext_ATLASPix_Trigger_M1 [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 ext_ATLASPix_Trigger_M1 ]
  set ext_ATLASPix_Trigger_M1ISO [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 ext_ATLASPix_Trigger_M1ISO ]
  set ext_ATLASPix_Trigger_M2 [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 ext_ATLASPix_Trigger_M2 ]

  # Create ports
  set ATLASPix_PLL_CLK_N [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_PLL_CLK_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ATLASPix_PLL_CLK_N
  set ATLASPix_PLL_CLK_P [ create_bd_port -dir O -from 0 -to 0 -type clk ATLASPix_PLL_CLK_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ATLASPix_PLL_CLK_P
  set ext_ATLASPix_Sync_reset_M1ISO_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1ISO_N ]
  set ext_ATLASPix_Sync_reset_M1ISO_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1ISO_P ]
  set ext_ATLASPix_Sync_reset_M1_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1_N ]
  set ext_ATLASPix_Sync_reset_M1_P_1 [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M1_P_1 ]
  set ext_ATLASPix_Sync_reset_M2_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M2_N ]
  set ext_ATLASPix_Sync_reset_M2_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ATLASPix_Sync_reset_M2_P ]
  set ext_ck1_diff_N [ create_bd_port -dir O -from 2 -to 0 -type clk ext_ck1_diff_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ck1_diff_N
  set ext_ck1_diff_P [ create_bd_port -dir O -from 2 -to 0 -type clk ext_ck1_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ck1_diff_P
  set ext_ck2_diff_N [ create_bd_port -dir O -from 2 -to 0 -type clk ext_ck2_diff_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ck2_diff_N
  set ext_ck2_diff_P [ create_bd_port -dir O -from 2 -to 0 -type clk ext_ck2_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ck2_diff_P
  set ext_ldM1ISO_diff_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM1ISO_diff_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM1ISO_diff_N
  set ext_ldM1ISO_diff_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM1ISO_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM1ISO_diff_P
  set ext_ldM1_diff_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM1_diff_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM1_diff_N
  set ext_ldM1_diff_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM1_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM1_diff_P
  set ext_ldM2_diff_N [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM2_diff_N ]
  set_property -dict [ list \
CONFIG.ASSOCIATED_RESET {ext_ATLASPix_Sync_reset_M1ISO} \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM2_diff_N
  set ext_ldM2_diff_P [ create_bd_port -dir O -from 0 -to 0 -type clk ext_ldM2_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_ldM2_diff_P
  set ext_sin_diff_N [ create_bd_port -dir O -from 2 -to 0 -type clk ext_sin_diff_N ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_sin_diff_N
  set ext_sin_diff_P [ create_bd_port -dir O -from 2 -to 0 -type clk ext_sin_diff_P ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {160000000} \
 ] $ext_sin_diff_P

  # Create instance: ATLASPix_FastControl
  create_hier_cell_ATLASPix_FastControl [current_bd_instance .] ATLASPix_FastControl

  # Create instance: ATLASPix_SR_FSM_top
  create_hier_cell_ATLASPix_SR_FSM_top [current_bd_instance .] ATLASPix_SR_FSM_top

  # Create instance: Caribou_control_0, and set properties
  set Caribou_control_0 [ create_bd_cell -type ip -vlnv CERN:user:Caribou_control:1.0 Caribou_control_0 ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.4 clk_wiz_0 ]
  set_property -dict [ list \
CONFIG.CLKIN1_JITTER_PS {50.0} \
CONFIG.CLKOUT1_JITTER {102.489} \
CONFIG.CLKOUT1_PHASE_ERROR {89.971} \
CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {160} \
CONFIG.CLK_IN1_BOARD_INTERFACE {Custom} \
CONFIG.MMCM_CLKFBOUT_MULT_F {5.000} \
CONFIG.MMCM_CLKIN1_PERIOD {5.000} \
CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F {6.250} \
CONFIG.MMCM_DIVCLK_DIVIDE {1} \
CONFIG.PRIM_SOURCE {Single_ended_clock_capable_pin} \
CONFIG.RESET_BOARD_INTERFACE {reset} \
CONFIG.USE_LOCKED {false} \
CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {800.000000} \
CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {25.000000} \
CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {200.000000} \
CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {50.000000} \
CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {50.000000} \
CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {133.333344} \
CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {800} \
CONFIG.PCW_ARMPLL_CTRL_FBDIV {48} \
CONFIG.PCW_CLK0_FREQ {200000000} \
CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1600.000} \
CONFIG.PCW_DDR_RAM_HIGHADDR {0x3FFFFFFF} \
CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {8} \
CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {5} \
CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {100 Mbps} \
CONFIG.PCW_ENET0_RESET_ENABLE {1} \
CONFIG.PCW_ENET0_RESET_IO {MIO 47} \
CONFIG.PCW_ENET_RESET_ENABLE {1} \
CONFIG.PCW_ENET_RESET_SELECT {Share reset pin} \
CONFIG.PCW_EN_CLK1_PORT {0} \
CONFIG.PCW_EN_EMIO_TTC0 {1} \
CONFIG.PCW_EN_ENET0 {1} \
CONFIG.PCW_EN_GPIO {1} \
CONFIG.PCW_EN_I2C0 {1} \
CONFIG.PCW_EN_QSPI {1} \
CONFIG.PCW_EN_SDIO0 {1} \
CONFIG.PCW_EN_TTC0 {1} \
CONFIG.PCW_EN_UART1 {1} \
CONFIG.PCW_EN_USB0 {1} \
CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {5} \
CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {1} \
CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {200} \
CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {64} \
CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51} \
CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_I2C0_RESET_ENABLE {1} \
CONFIG.PCW_I2C0_RESET_IO {MIO 46} \
CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {133.333328} \
CONFIG.PCW_I2C_RESET_ENABLE {1} \
CONFIG.PCW_I2C_RESET_SELECT {Share reset pin} \
CONFIG.PCW_IOPLL_CTRL_FBDIV {30} \
CONFIG.PCW_IO_IO_PLL_FREQMHZ {1000.000} \
CONFIG.PCW_IRQ_F2P_INTR {1} \
CONFIG.PCW_MIO_0_DIRECTION {out} \
CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_0_PULLUP {enabled} \
CONFIG.PCW_MIO_0_SLEW {slow} \
CONFIG.PCW_MIO_10_DIRECTION {inout} \
CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_10_PULLUP {enabled} \
CONFIG.PCW_MIO_10_SLEW {slow} \
CONFIG.PCW_MIO_11_DIRECTION {inout} \
CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_11_PULLUP {enabled} \
CONFIG.PCW_MIO_11_SLEW {slow} \
CONFIG.PCW_MIO_12_DIRECTION {inout} \
CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_12_PULLUP {enabled} \
CONFIG.PCW_MIO_12_SLEW {slow} \
CONFIG.PCW_MIO_13_DIRECTION {inout} \
CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_13_PULLUP {enabled} \
CONFIG.PCW_MIO_13_SLEW {slow} \
CONFIG.PCW_MIO_14_DIRECTION {in} \
CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_14_PULLUP {enabled} \
CONFIG.PCW_MIO_14_SLEW {slow} \
CONFIG.PCW_MIO_15_DIRECTION {in} \
CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_15_PULLUP {enabled} \
CONFIG.PCW_MIO_15_SLEW {slow} \
CONFIG.PCW_MIO_16_DIRECTION {out} \
CONFIG.PCW_MIO_16_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_16_PULLUP {disabled} \
CONFIG.PCW_MIO_16_SLEW {slow} \
CONFIG.PCW_MIO_17_DIRECTION {out} \
CONFIG.PCW_MIO_17_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_17_PULLUP {disabled} \
CONFIG.PCW_MIO_17_SLEW {slow} \
CONFIG.PCW_MIO_18_DIRECTION {out} \
CONFIG.PCW_MIO_18_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_18_PULLUP {disabled} \
CONFIG.PCW_MIO_18_SLEW {slow} \
CONFIG.PCW_MIO_19_DIRECTION {out} \
CONFIG.PCW_MIO_19_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_19_PULLUP {disabled} \
CONFIG.PCW_MIO_19_SLEW {slow} \
CONFIG.PCW_MIO_1_DIRECTION {out} \
CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_1_PULLUP {enabled} \
CONFIG.PCW_MIO_1_SLEW {slow} \
CONFIG.PCW_MIO_20_DIRECTION {out} \
CONFIG.PCW_MIO_20_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_20_PULLUP {disabled} \
CONFIG.PCW_MIO_20_SLEW {slow} \
CONFIG.PCW_MIO_21_DIRECTION {out} \
CONFIG.PCW_MIO_21_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_21_PULLUP {disabled} \
CONFIG.PCW_MIO_21_SLEW {slow} \
CONFIG.PCW_MIO_22_DIRECTION {in} \
CONFIG.PCW_MIO_22_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_22_PULLUP {disabled} \
CONFIG.PCW_MIO_22_SLEW {slow} \
CONFIG.PCW_MIO_23_DIRECTION {in} \
CONFIG.PCW_MIO_23_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_23_PULLUP {disabled} \
CONFIG.PCW_MIO_23_SLEW {slow} \
CONFIG.PCW_MIO_24_DIRECTION {in} \
CONFIG.PCW_MIO_24_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_24_PULLUP {disabled} \
CONFIG.PCW_MIO_24_SLEW {slow} \
CONFIG.PCW_MIO_25_DIRECTION {in} \
CONFIG.PCW_MIO_25_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_25_PULLUP {disabled} \
CONFIG.PCW_MIO_25_SLEW {slow} \
CONFIG.PCW_MIO_26_DIRECTION {in} \
CONFIG.PCW_MIO_26_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_26_PULLUP {disabled} \
CONFIG.PCW_MIO_26_SLEW {slow} \
CONFIG.PCW_MIO_27_DIRECTION {in} \
CONFIG.PCW_MIO_27_IOTYPE {HSTL 1.8V} \
CONFIG.PCW_MIO_27_PULLUP {disabled} \
CONFIG.PCW_MIO_27_SLEW {slow} \
CONFIG.PCW_MIO_28_DIRECTION {inout} \
CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_28_PULLUP {disabled} \
CONFIG.PCW_MIO_28_SLEW {slow} \
CONFIG.PCW_MIO_29_DIRECTION {in} \
CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_29_PULLUP {disabled} \
CONFIG.PCW_MIO_29_SLEW {slow} \
CONFIG.PCW_MIO_2_DIRECTION {inout} \
CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_2_PULLUP {disabled} \
CONFIG.PCW_MIO_2_SLEW {slow} \
CONFIG.PCW_MIO_30_DIRECTION {out} \
CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_30_PULLUP {disabled} \
CONFIG.PCW_MIO_30_SLEW {slow} \
CONFIG.PCW_MIO_31_DIRECTION {in} \
CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_31_PULLUP {disabled} \
CONFIG.PCW_MIO_31_SLEW {slow} \
CONFIG.PCW_MIO_32_DIRECTION {inout} \
CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_32_PULLUP {disabled} \
CONFIG.PCW_MIO_32_SLEW {slow} \
CONFIG.PCW_MIO_33_DIRECTION {inout} \
CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_33_PULLUP {disabled} \
CONFIG.PCW_MIO_33_SLEW {slow} \
CONFIG.PCW_MIO_34_DIRECTION {inout} \
CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_34_PULLUP {disabled} \
CONFIG.PCW_MIO_34_SLEW {slow} \
CONFIG.PCW_MIO_35_DIRECTION {inout} \
CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_35_PULLUP {disabled} \
CONFIG.PCW_MIO_35_SLEW {slow} \
CONFIG.PCW_MIO_36_DIRECTION {in} \
CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_36_PULLUP {disabled} \
CONFIG.PCW_MIO_36_SLEW {slow} \
CONFIG.PCW_MIO_37_DIRECTION {inout} \
CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_37_PULLUP {disabled} \
CONFIG.PCW_MIO_37_SLEW {slow} \
CONFIG.PCW_MIO_38_DIRECTION {inout} \
CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_38_PULLUP {disabled} \
CONFIG.PCW_MIO_38_SLEW {slow} \
CONFIG.PCW_MIO_39_DIRECTION {inout} \
CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_39_PULLUP {disabled} \
CONFIG.PCW_MIO_39_SLEW {slow} \
CONFIG.PCW_MIO_3_DIRECTION {inout} \
CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_3_PULLUP {disabled} \
CONFIG.PCW_MIO_3_SLEW {slow} \
CONFIG.PCW_MIO_40_DIRECTION {inout} \
CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_40_PULLUP {disabled} \
CONFIG.PCW_MIO_40_SLEW {slow} \
CONFIG.PCW_MIO_41_DIRECTION {inout} \
CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_41_PULLUP {disabled} \
CONFIG.PCW_MIO_41_SLEW {slow} \
CONFIG.PCW_MIO_42_DIRECTION {inout} \
CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_42_PULLUP {disabled} \
CONFIG.PCW_MIO_42_SLEW {slow} \
CONFIG.PCW_MIO_43_DIRECTION {inout} \
CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_43_PULLUP {disabled} \
CONFIG.PCW_MIO_43_SLEW {slow} \
CONFIG.PCW_MIO_44_DIRECTION {inout} \
CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_44_PULLUP {disabled} \
CONFIG.PCW_MIO_44_SLEW {slow} \
CONFIG.PCW_MIO_45_DIRECTION {inout} \
CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_45_PULLUP {disabled} \
CONFIG.PCW_MIO_45_SLEW {slow} \
CONFIG.PCW_MIO_46_DIRECTION {out} \
CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_46_PULLUP {enabled} \
CONFIG.PCW_MIO_46_SLEW {slow} \
CONFIG.PCW_MIO_47_DIRECTION {out} \
CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_47_PULLUP {enabled} \
CONFIG.PCW_MIO_47_SLEW {slow} \
CONFIG.PCW_MIO_48_DIRECTION {out} \
CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_48_PULLUP {disabled} \
CONFIG.PCW_MIO_48_SLEW {slow} \
CONFIG.PCW_MIO_49_DIRECTION {in} \
CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_49_PULLUP {disabled} \
CONFIG.PCW_MIO_49_SLEW {slow} \
CONFIG.PCW_MIO_4_DIRECTION {inout} \
CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_4_PULLUP {disabled} \
CONFIG.PCW_MIO_4_SLEW {slow} \
CONFIG.PCW_MIO_50_DIRECTION {inout} \
CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_50_PULLUP {enabled} \
CONFIG.PCW_MIO_50_SLEW {slow} \
CONFIG.PCW_MIO_51_DIRECTION {inout} \
CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_51_PULLUP {enabled} \
CONFIG.PCW_MIO_51_SLEW {slow} \
CONFIG.PCW_MIO_52_DIRECTION {out} \
CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_52_PULLUP {disabled} \
CONFIG.PCW_MIO_52_SLEW {slow} \
CONFIG.PCW_MIO_53_DIRECTION {inout} \
CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_53_PULLUP {disabled} \
CONFIG.PCW_MIO_53_SLEW {slow} \
CONFIG.PCW_MIO_5_DIRECTION {inout} \
CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_5_PULLUP {disabled} \
CONFIG.PCW_MIO_5_SLEW {slow} \
CONFIG.PCW_MIO_6_DIRECTION {out} \
CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_6_PULLUP {disabled} \
CONFIG.PCW_MIO_6_SLEW {slow} \
CONFIG.PCW_MIO_7_DIRECTION {out} \
CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_7_PULLUP {disabled} \
CONFIG.PCW_MIO_7_SLEW {slow} \
CONFIG.PCW_MIO_8_DIRECTION {out} \
CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_8_PULLUP {disabled} \
CONFIG.PCW_MIO_8_SLEW {slow} \
CONFIG.PCW_MIO_9_DIRECTION {out} \
CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 1.8V} \
CONFIG.PCW_MIO_9_PULLUP {enabled} \
CONFIG.PCW_MIO_9_SLEW {slow} \
CONFIG.PCW_MIO_TREE_PERIPHERALS {Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#USB Reset#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#SD 0#SD 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#I2C Reset#ENET Reset#UART 1#UART 1#I2C 0#I2C 0#Enet 0#Enet 0} \
CONFIG.PCW_MIO_TREE_SIGNALS {qspi1_ss_b#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]#qspi0_sclk#reset#qspi_fbclk#qspi1_sclk#qspi1_io[0]#qspi1_io[1]#qspi1_io[2]#qspi1_io[3]#cd#wp#tx_clk#txd[0]#txd[1]#txd[2]#txd[3]#tx_ctl#rx_clk#rxd[0]#rxd[1]#rxd[2]#rxd[3]#rx_ctl#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#reset#reset#tx#rx#scl#sda#mdc#mdio} \
CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {5} \
CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 1.8V} \
CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {1} \
CONFIG.PCW_QSPI_GRP_FBCLK_IO {MIO 8} \
CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} \
CONFIG.PCW_QSPI_GRP_IO1_IO {MIO 0 9 .. 13} \
CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFDFFFFFF} \
CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {5} \
CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
CONFIG.PCW_SD0_GRP_CD_ENABLE {1} \
CONFIG.PCW_SD0_GRP_CD_IO {MIO 14} \
CONFIG.PCW_SD0_GRP_WP_ENABLE {1} \
CONFIG.PCW_SD0_GRP_WP_IO {MIO 15} \
CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {20} \
CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {50} \
CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49} \
CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {20} \
CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {50} \
CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.521} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.636} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.54} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.621} \
CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {2048 MBits} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.226} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.278} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.184} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.309} \
CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333313} \
CONFIG.PCW_UIPARAM_DDR_PARTNO {Custom} \
CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {36.0} \
CONFIG.PCW_UIPARAM_DDR_T_RC {49.5} \
CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {1} \
CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NONE} \
CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_USB0_RESET_ENABLE {1} \
CONFIG.PCW_USB0_RESET_IO {MIO 7} \
CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
CONFIG.PCW_USB_RESET_ENABLE {1} \
CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
CONFIG.PCW_USE_DMA0 {0} \
CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
CONFIG.PCW_USE_M_AXI_GP0 {1} \
CONFIG.PCW_USE_S_AXI_GP0 {0} \
CONFIG.PCW_USE_S_AXI_HP0 {1} \
CONFIG.PCW_USE_S_AXI_HP1 {0} \
CONFIG.preset {ZC706} \
 ] $processing_system7_0

  # Create instance: ps7_0_axi_periph, and set properties
  set ps7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 ps7_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {6} \
 ] $ps7_0_axi_periph

  # Create instance: rst_ps7_0_100M, and set properties
  set rst_ps7_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_ps7_0_100M ]

  # Create interface connections
  connect_bd_intf_net -intf_net CLK_IN_D_1 [get_bd_intf_ports ext_ATLASPix_Trigger_M2] [get_bd_intf_pins ATLASPix_FastControl/CLK_IN_D2]
  connect_bd_intf_net -intf_net CLK_IN_D_1_1 [get_bd_intf_ports ext_ATLASPix_Trigger_M1] [get_bd_intf_pins ATLASPix_FastControl/CLK_IN_D]
  connect_bd_intf_net -intf_net CLK_IN_D_2_1 [get_bd_intf_ports ext_ATLASPix_Trigger_M1ISO] [get_bd_intf_pins ATLASPix_FastControl/CLK_IN_D1]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins ATLASPix_SR_FSM_top/S00_AXI] [get_bd_intf_pins ps7_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins ps7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins Caribou_control_0/axi] [get_bd_intf_pins ps7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M03_AXI [get_bd_intf_pins ATLASPix_FastControl/S00_AXI] [get_bd_intf_pins ps7_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M04_AXI [get_bd_intf_pins ATLASPix_FastControl/S00_AXI1] [get_bd_intf_pins ps7_0_axi_periph/M04_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M05_AXI [get_bd_intf_pins ATLASPix_FastControl/S00_AXI2] [get_bd_intf_pins ps7_0_axi_periph/M05_AXI]

  # Create port connections
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_N [get_bd_ports ext_ATLASPix_Sync_reset_M2_N] [get_bd_pins ATLASPix_FastControl/OBUF_DS_N]
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_N1 [get_bd_ports ext_ATLASPix_Sync_reset_M1_N] [get_bd_pins ATLASPix_FastControl/OBUF_DS_N1]
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_N2 [get_bd_ports ext_ATLASPix_Sync_reset_M1ISO_N] [get_bd_pins ATLASPix_FastControl/OBUF_DS_N2]
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_P [get_bd_ports ext_ATLASPix_Sync_reset_M2_P] [get_bd_pins ATLASPix_FastControl/OBUF_DS_P]
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_P1 [get_bd_ports ext_ATLASPix_Sync_reset_M1_P_1] [get_bd_pins ATLASPix_FastControl/OBUF_DS_P1]
  connect_bd_net -net ATLASPix_FastControl_OBUF_DS_P2 [get_bd_ports ext_ATLASPix_Sync_reset_M1ISO_P] [get_bd_pins ATLASPix_FastControl/OBUF_DS_P2]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins ATLASPix_SR_FSM_top/OBUF_IN] [get_bd_pins clk_wiz_0/clk_out1]
  connect_bd_net -net output_SC_ck1_array_N [get_bd_ports ext_ck1_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/ck1_array_N]
  connect_bd_net -net output_SC_ck1_array_P [get_bd_ports ext_ck1_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/ck1_array_P]
  connect_bd_net -net output_SC_ck2_array_N [get_bd_ports ext_ck2_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/ck2_array_N]
  connect_bd_net -net output_SC_ck2_array_P [get_bd_ports ext_ck2_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/ck2_array_P]
  connect_bd_net -net output_SC_ld_m1_N [get_bd_ports ext_ldM1_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/ld_m1_N]
  connect_bd_net -net output_SC_ld_m1_P [get_bd_ports ext_ldM1_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/ld_m1_P]
  connect_bd_net -net output_SC_ld_m1iso_N [get_bd_ports ext_ldM1ISO_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/ld_m1iso_N]
  connect_bd_net -net output_SC_ld_m1iso_P [get_bd_ports ext_ldM1ISO_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/ld_m1iso_P]
  connect_bd_net -net output_SC_ld_m2_N [get_bd_ports ext_ldM2_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/ld_m2_N]
  connect_bd_net -net output_SC_ld_m2_P [get_bd_ports ext_ldM2_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/ld_m2_P]
  connect_bd_net -net output_SC_pll_clk_N [get_bd_ports ATLASPix_PLL_CLK_N] [get_bd_pins ATLASPix_SR_FSM_top/pll_clk_N]
  connect_bd_net -net output_SC_pll_clk_P [get_bd_ports ATLASPix_PLL_CLK_P] [get_bd_pins ATLASPix_SR_FSM_top/pll_clk_P]
  connect_bd_net -net output_SC_sin_array_N [get_bd_ports ext_sin_diff_N] [get_bd_pins ATLASPix_SR_FSM_top/sin_array_N]
  connect_bd_net -net output_SC_sin_array_P [get_bd_ports ext_sin_diff_P] [get_bd_pins ATLASPix_SR_FSM_top/sin_array_P]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins ATLASPix_FastControl/s00_axi_aclk] [get_bd_pins ATLASPix_SR_FSM_top/s00_axi_aclk] [get_bd_pins Caribou_control_0/aclk] [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins ps7_0_axi_periph/ACLK] [get_bd_pins ps7_0_axi_periph/M00_ACLK] [get_bd_pins ps7_0_axi_periph/M01_ACLK] [get_bd_pins ps7_0_axi_periph/M02_ACLK] [get_bd_pins ps7_0_axi_periph/M03_ACLK] [get_bd_pins ps7_0_axi_periph/M04_ACLK] [get_bd_pins ps7_0_axi_periph/M05_ACLK] [get_bd_pins ps7_0_axi_periph/S00_ACLK] [get_bd_pins rst_ps7_0_100M/slowest_sync_clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_ps7_0_100M/ext_reset_in]
  connect_bd_net -net rst_ps7_0_100M_interconnect_aresetn [get_bd_pins ATLASPix_SR_FSM_top/s00_axi_aresetn] [get_bd_pins Caribou_control_0/aresetN] [get_bd_pins ps7_0_axi_periph/ARESETN] [get_bd_pins ps7_0_axi_periph/M00_ARESETN] [get_bd_pins ps7_0_axi_periph/M01_ARESETN] [get_bd_pins ps7_0_axi_periph/M02_ARESETN] [get_bd_pins ps7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_ps7_0_100M/interconnect_aresetn]
  connect_bd_net -net rst_ps7_0_100M_peripheral_aresetn [get_bd_pins ATLASPix_FastControl/s00_axi_aresetn] [get_bd_pins ps7_0_axi_periph/M03_ARESETN] [get_bd_pins ps7_0_axi_periph/M04_ARESETN] [get_bd_pins ps7_0_axi_periph/M05_ARESETN] [get_bd_pins rst_ps7_0_100M/peripheral_aresetn]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ATLASPix_SR_FSM_top/ATLASPix_SR_FSM_0/S00_AXI/S00_AXI_reg] SEG_ATLASPix_SR_FSM_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00001000 -offset 0x43C30000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs Caribou_control_0/interface_aximm/reg0] SEG_Caribou_control_0_reg0


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


